FROM registry.gitlab.com/dedyms/node:14-dev AS tukang

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV APPRISE_CONFIG_DIR /config
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

WORKDIR $HOME
# Install nginx and supervisord
RUN apt-get update -qq && \
    apt-get install -y -qq --no-install-recommends build-essential libffi-dev libssl-dev python3-dev python3-pip python3-setuptools git
USER $CONTAINERUSER
RUN git clone --depth=1 https://github.com/caronc/apprise-api.git apprise
RUN pip3 install --user --no-cache-dir -r ./apprise/requirements.txt gunicorn


FROM registry.gitlab.com/dedyms/nginx:mainline
USER root
RUN apt update && apt install -y --no-install-recommends \
    python3-minimal libffi7 libssl1.1 python3-setuptools supervisor && \
    apt clean && \
    rm -rf /var/lib/apt/*

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV APPRISE_CONFIG_DIR $HOME/apprise/config
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1

COPY --from=tukang --chown=$CONTAINERUSER:$CONTAINERUSER $HOME/.local/ $HOME/.local/
COPY --from=tukang --chown=$CONTAINERUSER:$CONTAINERUSER $HOME/apprise/apprise_api $HOME/apprise/apprise_api
COPY --from=tukang --chown=$CONTAINERUSER:$CONTAINERUSER $HOME/apprise/apprise_api/static /var/www/html/s/
COPY gunicorn.conf.py $HOME/apprise/apprise_api/gunicorn.conf.py
COPY supervisord.conf $HOME/supervisord.conf
COPY nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 8000
USER $CONTAINERUSER
WORKDIR $HOME/apprise/apprise_api
VOLUME $HOME/apprise/config
CMD ["supervisord","-c","/home/debian/supervisord.conf"]
#CMD ["gunicorn", "-c", "./gunicorn.conf.py", "--worker-tmp-dir", "/dev/shm", "core.wsgi"]

